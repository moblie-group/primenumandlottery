import 'dart:io';

void func1(var num) {
  int i, m = 0, t = 0;
  m = num ~/ (2);
  for (i = 2; i <= m; i++) {
    if (num % i == 0) {
      print('$num is not a prime number');
      t = 1;
      break;
    }
  }
  if (t == 0) {
    print('$num is prime number');
  }
}

void main() {
  print("Number: ");
  int num;
  num = int.parse(stdin.readLineSync()!);
  func1(num);
}
